FROM python:3.10-alpine
RUN pip install fastapi

RUN mkdir /app
WORKDIR /app
COPY app.py app.py

ENTRYPOINT uvicorn app:app --host 0.0.0.0
