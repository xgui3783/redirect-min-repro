import os
from fastapi import FastAPI, BackgroundTasks
from fastapi.responses import RedirectResponse

app = FastAPI()

redir = os.getenv("REDIR", "https://lab.ebrains.eu/")

@app.get("/")
@app.get("/get")
def get():
    return "get"

@app.post("/post")
def post():
    return "post"

def ping_me(foo):
    import time
    time.sleep(10)
    print(f"awake {foo}")

@app.get("/redirect")
def redirect(background_tasks: BackgroundTasks):
    background_tasks.add_task(ping_me, redir)
    print(f"redirect {redir}")
    return RedirectResponse(redir)
